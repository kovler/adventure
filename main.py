from bottle import route, run, template, static_file, get, post, request
import json
import MySQLdb


db = MySQLdb.connect(host="localhost", user="root", passwd="", db="adventure")
cur = db.cursor()


def create_user(user_name):
    id = 1
    name = user_name
    gold = 10
    life = 100
    current_page = 1
    cur.execute('insert into user(id, name, gold, life, current_page) values(%d, "%s", %d, %d, %d);' % (id, name, gold, life, current_page))


@get("/")
def index():
    return template("adventure.html")


@post("/start")
def start():
    user_name = str(request.params.user)
    current_adv_id = int(request.params.adventure_id)
    cur.execute('select * from user where name="' + user_name + '";')
    current_user = cur.fetchall()
    if len(current_user) == 0:
        create_user(user_name)
        cur.execute('select * from user where name="' + user_name + '";')
        current_user = cur.fetchall()
    current_page_id = int(current_user[0][4])
    cur.execute('select question, image, the_option, result, next_destination from page join options on (options.page_id = page.id) where page.id=' + str(current_page_id) + ';')
    page = cur.fetchall()

    #todo add the next step based on db
    return json.dumps({"user": current_user[0][0],
                       "adventure": current_adv_id,
                       "current": current_page_id,
                       "text": page[0][0],
                       "image": page[0][1],
                       "option_1": [page[0][2], page[0][3], page[0][4]],
                       "option_2": [page[1][2], page[1][3], page[1][4]],
                       "option_3": [page[2][2], page[2][3], page[2][4]],
                       "option_4": [page[3][2], page[3][3], page[3][4]]
                       })


@post("/story")
def story():
    user_id = request.forms.get('user')
    # current_adv_id = request.forms.get('adventure')
    new_page_id = request.forms.get('next')
# for the update score+liife functions
    # cur.execute('UPDATE `user` SET `current_page`=[value-3],`life`=[value-4],`coins`=[value-5] WHERE 1;')
    cur.execute('select * from user where id="' + user_id + '";')
    current_user = cur.fetchall()
    cur.execute('select question, image, the_option, result, next_destination from page join options on (options.page_id = page.id) where page.id=' + str(new_page_id) + ';')
    page = cur.fetchall()
    #todo add the next step based on db
    return json.dumps({"user": current_user[0][0],
                       "adventure": new_page_id,
                       "current": new_page_id,
                       "text": page[0][0],
                       "image": page[0][1],
                       "option_1": [page[0][2], page[0][3], page[0][4]],
                       "option_2": [page[1][2], page[1][3], page[1][4]],
                       "option_3": [page[2][2], page[2][3], page[2][4]],
                       "option_4": [page[3][2], page[3][3], page[3][4]]
                       })


@post("/update_user_score")
def update_user_score():
    user_id = str(request.params.user)
    user_result= str(request.params.result)
    cur.execute('select coins from user where id="' + user_id + '";')
    current_score = cur.fetchall()
    current_score += user_result
    cur.execute('UPDATE `user` SET  `coins`="' + current_score+ '" WHERE id="' + user_id + '";')
    #todo add the next step based on db
    return json.dumps({"current_score": current_score})


@get('/js/<filename:re:.*\.js>')
def javascripts(filename):
    return static_file(filename, root='js')


@get('/css/<filename:re:.*\.css>')
def stylesheets(filename):
    return static_file(filename, root='css')


@get('/images/<filename:re:.*\.(jpg|png|gif|ico)>')
def images(filename):
    return static_file(filename, root='images')


run(host='localhost', port=7000)
