-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2016 at 12:39 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `adventure`
--

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE IF NOT EXISTS `options` (
  `page_id` int(11) NOT NULL,
  `option_id` int(11) NOT NULL,
  `the_option` varchar(255) NOT NULL,
  `result` varchar(255) NOT NULL,
  `next_destination` int(11) NOT NULL,
  UNIQUE KEY `page_id` (`page_id`,`option_id`),
  UNIQUE KEY `page_id_2` (`page_id`,`option_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `options`
--

INSERT INTO `options` (`page_id`, `option_id`, `the_option`, `result`, `next_destination`) VALUES
(1, 1, 'i''ll fight like Mohamad Ali', '[[20,20],[-20,-20]]', 2),
(1, 2, 'Please don''t touch me! take 10 coins!', '[[-10]]', 3),
(1, 3, 'I''ll use my diplomatic abilities and will begg for my life.', '[[0,5] [0,-5]]', 3),
(1, 4, 'I''ll run like speedy Gonzales ! ', '[[5,15] [-5,-15]]', 2),
(2, 1, 'I''ll use my sout skills for buding an awsome rafsoda!', '[[10,-5]]', 1),
(2, 2, 'i''ll swim and will hope not to get cold/drown.', '[[20,-10]]', 2),
(2, 3, 'I''ll just wait until the next adventure to arrive.. :/ ZzZzzz', '[[0,0]]', 3),
(2, 4, 'I''ll find a shark, become his fellow, and ask for help (:', '[[20,10],[-20,-10]]', 1),
(3, 1, 'I''ll lie that I''m the diller of the lion''s king.', '[[-15,-15], [15,15]]', 2),
(3, 2, 'i''ll fight and then i''ll cook him like a shnitzel!!', '[[20,20],[-20,-20]]', 1),
(3, 3, 'What''s the problem dude? we''ll become friends and will go to hunt bambies together (;', '[[20,10],[-20,-10]]', 1),
(3, 4, 'I''ll run away, daa..', '[[15,0],[0,-15]]', 2);

-- --------------------------------------------------------

--
-- Table structure for table `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `picture` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page`
--

INSERT INTO `page` (`id`, `question`, `picture`) VALUES
(1, 'you meet a monster..', 1),
(2, 'you stand infront a river..', 2),
(3, 'all of a sudden, you see a tiger..', 3);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `name` varchar(60) NOT NULL,
  `current_page` int(11) NOT NULL DEFAULT '0',
  `life` int(11) NOT NULL,
  `coins` int(11) NOT NULL,
  PRIMARY KEY (`id`,`name`),
  UNIQUE KEY `id` (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`page_id`) REFERENCES `page` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
